# nirvana-typescript-workspace

An opinionated [Node.js] + [TypeScript] project starter. There are many like it, but this one is mine.

[Node.js]: https://nodejs.org/
[TypeScript]: https://www.typescriptlang.org/

## Development

### Install prerequisites

- [Node.js]
- [pnpm]
- [pre-commit]

[pnpm]: https://pnpm.io/
[pre-commit]: https://pre-commit.com/

### Install the pre-commit hook

```sh
pre-commit install
```

This installs a Git hook that runs a quick sanity check before every commit.

### Install dependencies

```sh
pnpm install
```

### Run the app

```sh
pnpm start
```

### Run the app with autoreload

This will watch for changes to source files and kill/restart the app when any are changed.

```sh
pnpm dev
```

### Run the tests

```sh
pnpm test
```
